### JSON Libraries Benchmark ###

This project is meant to test out the performance of Gson, Jackson and Moshi compared to JSONObject.

[Click here to view the publication on Medium.com](https://medium.com/@dannydamsky99/heres-why-you-probably-shouldn-t-be-using-the-gson-library-in-2018-4bed5698b78b)