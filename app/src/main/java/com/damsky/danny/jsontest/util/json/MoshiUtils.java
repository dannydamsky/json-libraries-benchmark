package com.damsky.danny.jsontest.util.json;

import android.support.annotation.NonNull;

import com.damsky.danny.jsontest.data.ComplexParentObject;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;

/**
 * Utility class for converting objects to and from JSON using the {@link Moshi} implementation.
 *
 * @author Danny Damsky
 */
public final class MoshiUtils {

    private static Moshi moshi = new Moshi.Builder().build();
    private static final JsonAdapter<ComplexParentObject> complexParentObjectJsonAdapter = moshi.adapter(ComplexParentObject.class);

    public static String toJson(@NonNull final ComplexParentObject src) {
        return complexParentObjectJsonAdapter.toJson(src);
    }

    public static ComplexParentObject fromJson(@NonNull final String json) {
        try {
            return complexParentObjectJsonAdapter.fromJson(json);
        } catch (IOException e) {
            return new ComplexParentObject();
        }
    }
}
