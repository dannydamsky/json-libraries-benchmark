package com.damsky.danny.jsontest;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.damsky.danny.jsontest.data.ComplexParentObject;
import com.damsky.danny.jsontest.util.ComplexObjectGenerator;
import com.damsky.danny.jsontest.util.JsonBenchmark;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Danny Damsky
 */
public final class MainActivity extends AppCompatActivity {

    private static final ComplexParentObject OBJECT = ComplexObjectGenerator.buildComplexParentObject();
    private static final ComplexParentObject OBJECT1 = ComplexObjectGenerator.buildComplexParentObject();

    private boolean b = true;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final TextView results = findViewById(R.id.results);

        final FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            if (b) {
                Observable.just(OBJECT)
                        .map(JsonBenchmark::new)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(jsonBenchmark -> results.setText(jsonBenchmark.toString()));
            } else {
                Observable.just(OBJECT1)
                        .map(JsonBenchmark::new)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(jsonBenchmark -> results.setText(jsonBenchmark.toString()));
            }
            b = !b;
        });
    }
}
