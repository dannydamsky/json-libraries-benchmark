package com.damsky.danny.jsontest.util;

import android.support.annotation.NonNull;

import com.damsky.danny.jsontest.data.ComplexParentObject;
import com.damsky.danny.jsontest.util.json.GsonUtils;
import com.damsky.danny.jsontest.util.json.JacksonUtils;
import com.damsky.danny.jsontest.util.json.JsonUtils;
import com.damsky.danny.jsontest.util.json.MoshiUtils;

/**
 * This class is used for running benchmarks to test the performance of various JSON parsing
 * solutions.
 *
 * @author Danny Damsky
 */
public final class JsonBenchmark {

    /*
    Deserialize: Time in milliseconds
    Serialize: Time in milliseconds
    JSON Length: How many characters the JSON String has.
    Object Length: How many characters are in the String from the ComplexParentObject's toString method.
     */

    private final long deserializeGson;
    private final long serializeGson;
    private final int gsonLength;
    private final int gsonObjectLength;

    private final long deserializeMoshi;
    private final long serializeMoshi;
    private final int moshiLength;
    private final int moshiObjectLength;

    private final long deserializeJson;
    private final long serializeJson;
    private final int jsonLength;
    private final int jsonObjectLength;

    private final long deserializeJackson;
    private final long serializeJackson;
    private final long jacksonLength;
    private final long jacksonObjectLength;

    /**
     * The constructor of this class takes a complex JSON object and parses it using
     * the various JSON parsing solutions. In order to view the results of the benchmark
     * please refer to the {@link #toString() toString} method.
     *
     * @param object A complex object to be parsed by the JSON parsing solutions.
     */
    public JsonBenchmark(@NonNull final ComplexParentObject object) {

        // Serialization
        final long a1 = System.currentTimeMillis();
        final String gson = GsonUtils.toJson(object);
        final long a2 = System.currentTimeMillis();
        final String json = JsonUtils.toJson(object);
        final long a3 = System.currentTimeMillis();
        final String mson = MoshiUtils.toJson(object);
        final long a4 = System.currentTimeMillis();
        final String jackson = MoshiUtils.toJson(object);

        // Deserialization
        final long a5 = System.currentTimeMillis();
        final ComplexParentObject gsonObject = GsonUtils.fromJson(gson, ComplexParentObject.class);
        final long a6 = System.currentTimeMillis();
        final ComplexParentObject jsonObject = JsonUtils.fromJson(json);
        final long a7 = System.currentTimeMillis();
        final ComplexParentObject moshiObject = MoshiUtils.fromJson(mson);
        final long a8 = System.currentTimeMillis();
        final ComplexParentObject jacksonObject = JacksonUtils.fromJson(jackson, ComplexParentObject.class);
        final long a9 = System.currentTimeMillis();


        this.deserializeGson = a2 - a1;
        this.deserializeJson = a3 - a2;
        this.deserializeMoshi = a4 - a3;
        this.deserializeJackson = a5 - a4;
        this.serializeGson = a6 - a5;
        this.serializeJson = a7 - a6;
        this.serializeMoshi = a8 - a7;
        this.serializeJackson = a9 - a8;
        this.gsonLength = gson.length();
        this.jsonLength = json.length();
        this.moshiLength = mson.length();
        this.jacksonLength = jackson.length();
        this.gsonObjectLength = gsonObject.toString().length();
        this.jsonObjectLength = jsonObject.toString().length();
        this.moshiObjectLength = moshiObject.toString().length();
        this.jacksonObjectLength = jacksonObject.toString().length();
    }

    @Override
    public String toString() {
        return "Gson:\n\t" +
                "Deserialize: " + deserializeGson + "\n\t" +
                "Serialize: " + serializeGson + "\n\t" +
                "JSON Length: " + gsonLength + "\n\t" +
                "Object Length: " + gsonObjectLength + "\n" +
                "\nMoshi:\n\t" +
                "Deserialize: " + deserializeMoshi + "\n\t" +
                "Serialize: " + serializeMoshi + "\n\t" +
                "JSON Length: " + moshiLength + "\n\t" +
                "Object Length: " + moshiObjectLength + "\n" +
                "\nJSONObject:\n\t" +
                "Deserialize: " + deserializeJson + "\n\t" +
                "Serialize: " + serializeJson + "\n\t" +
                "JSON Length: " + jsonLength + "\n\t" +
                "Object Length: " + jsonObjectLength + "\n" +
                "\nJackson:\n\t" +
                "Deserialize: " + deserializeJackson + "\n\t" +
                "Serialize: " + serializeJackson + "\n\t" +
                "JSON Length: " + jacksonLength + "\n\t" +
                "Object Length: " + jacksonObjectLength;
    }
}
