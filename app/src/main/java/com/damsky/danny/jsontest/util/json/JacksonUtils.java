package com.damsky.danny.jsontest.util.json;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Utility class for converting objects to and from JSON using the Jackson implementation.
 *
 * @author Danny Damsky
 * @see ObjectMapper
 */
public final class JacksonUtils {

    private static final ObjectMapper mapper = new ObjectMapper();

    @NonNull
    public static String toJson(@NonNull final Object src) {
        try {
            return mapper.writeValueAsString(src);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    @Nullable
    public static <T> T fromJson(@NonNull final String json, @NonNull final Class<T> classOfT) {
        try {
            return mapper.readValue(json, classOfT);
        } catch (IOException e) {
            return null;
        }
    }

}
