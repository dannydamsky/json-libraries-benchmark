package com.damsky.danny.jsontest.util.json;

import android.support.annotation.NonNull;

import com.damsky.danny.jsontest.data.ComplexChildObject;
import com.damsky.danny.jsontest.data.ComplexParentObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for converting objects to and from JSON using the {@link JSONObject} implementation.
 *
 * @author Danny Damsky
 */
public final class JsonUtils {

    @NonNull
    public static String toJson(@NonNull final ComplexParentObject object) {
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put(ComplexParentObject.A, object.getA());
            jsonObject.put(ComplexParentObject.B, object.getB());
            jsonObject.put(ComplexParentObject.C, object.getC());
            jsonObject.put(ComplexParentObject.AA, object.getAa());
            jsonObject.put(ComplexParentObject.BB, object.getBb());
            jsonObject.put(ComplexParentObject.CC, object.getCc());
            jsonObject.put(ComplexParentObject.AAA, object.getAaa());
            jsonObject.put(ComplexParentObject.BBB, object.getBbb());
            jsonObject.put(ComplexParentObject.CCC, object.getCcc());

            final JSONArray jsonArray = new JSONArray();
            final List<ComplexChildObject> objects = object.getChildren();
            final int length = objects.size();

            for (int i = 0; i < length; i++) {
                final ComplexChildObject complexChildObject = objects.get(i);
                final JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put(ComplexChildObject.A, complexChildObject.getA());
                jsonObject1.put(ComplexChildObject.B, complexChildObject.getB());
                jsonObject1.put(ComplexChildObject.C, complexChildObject.getC());
                jsonObject1.put(ComplexChildObject.AA, complexChildObject.getAa());
                jsonObject1.put(ComplexChildObject.BB, complexChildObject.getBb());
                jsonObject1.put(ComplexChildObject.CC, complexChildObject.getCc());
                jsonObject1.put(ComplexChildObject.AAA, complexChildObject.getAaa());
                jsonObject1.put(ComplexChildObject.BBB, complexChildObject.getBbb());
                jsonObject1.put(ComplexChildObject.CCC, complexChildObject.getCcc());
                jsonObject1.put(ComplexChildObject.STRINGS, new JSONArray(complexChildObject.getStrings()));

                jsonArray.put(jsonObject1);
            }

            jsonObject.put(ComplexParentObject.CHILDREN, jsonArray);

            return jsonObject.toString();
        } catch (Exception e) {
            return "";
        }
    }

    @NonNull
    public static ComplexParentObject fromJson(@NonNull final String json) {
        try {
            final JSONObject jsonObject = new JSONObject(json);
            final ComplexParentObject complexParentObject = new ComplexParentObject();
            complexParentObject.setA(jsonObject.getString(ComplexParentObject.A));
            complexParentObject.setB(jsonObject.getString(ComplexParentObject.B));
            complexParentObject.setC(jsonObject.getString(ComplexParentObject.C));
            complexParentObject.setAa(jsonObject.getLong(ComplexParentObject.AA));
            complexParentObject.setBb(jsonObject.getLong(ComplexParentObject.BB));
            complexParentObject.setCc(jsonObject.getLong(ComplexParentObject.CC));
            complexParentObject.setAaa(jsonObject.getInt(ComplexParentObject.AAA));
            complexParentObject.setBbb(jsonObject.getInt(ComplexParentObject.BBB));
            complexParentObject.setCcc(jsonObject.getInt(ComplexParentObject.CCC));

            final JSONArray jsonArray = jsonObject.getJSONArray(ComplexParentObject.CHILDREN);
            final int length = jsonArray.length();
            final List<ComplexChildObject> childObjects = new ArrayList<>(length);

            for (int i = 0; i < length; i++) {
                final JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                final ComplexChildObject complexChildObject = new ComplexChildObject();
                complexChildObject.setA(jsonObject1.getString(ComplexChildObject.A));
                complexChildObject.setB(jsonObject1.getString(ComplexChildObject.B));
                complexChildObject.setC(jsonObject1.getString(ComplexChildObject.C));
                complexChildObject.setAa(jsonObject1.getLong(ComplexChildObject.AA));
                complexChildObject.setBb(jsonObject1.getLong(ComplexChildObject.BB));
                complexChildObject.setCc(jsonObject1.getLong(ComplexChildObject.CC));
                complexChildObject.setAaa(jsonObject1.getInt(ComplexChildObject.AAA));
                complexChildObject.setBbb(jsonObject1.getInt(ComplexChildObject.BBB));
                complexChildObject.setCcc(jsonObject1.getInt(ComplexChildObject.CCC));

                final JSONArray jsonArray1 = jsonObject1.getJSONArray(ComplexChildObject.STRINGS);
                final int length1 = jsonArray1.length();
                final List<String> childStrings = new ArrayList<>(length1);

                for (int j = 0; j < length1; j++) {
                    childStrings.add(jsonArray1.getString(j));
                }

                complexChildObject.setStrings(childStrings);
                childObjects.add(complexChildObject);
            }

            complexParentObject.setChildren(childObjects);

            return complexParentObject;
        } catch (Exception e) {
            return new ComplexParentObject();
        }
    }

    private JsonUtils() {
    }

}
