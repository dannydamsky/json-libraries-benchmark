package com.damsky.danny.jsontest.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import com.squareup.moshi.Json;

import java.util.List;

/**
 * @author Danny Damsky
 * @see ComplexParentObject
 */
public final class ComplexChildObject {

    public static final String A = "a";
    public static final String B = "b";
    public static final String C = "c";
    public static final String AA = "aa";
    public static final String BB = "bb";
    public static final String CC = "cc";
    public static final String AAA = "aaa";
    public static final String BBB = "bbb";
    public static final String CCC = "ccc";
    public static final String STRINGS = "strings";

    @SerializedName(A)
    @Json(name = A)
    @JsonProperty(A)
    private String a;
    @SerializedName(B)
    @Json(name = B)
    @JsonProperty(B)
    private String b;
    @SerializedName(C)
    @Json(name = C)
    @JsonProperty(C)
    private String c;
    @SerializedName(AA)
    @Json(name = AA)
    @JsonProperty(AA)
    private long aa;
    @SerializedName(BB)
    @Json(name = BB)
    @JsonProperty(BB)
    private long bb;
    @SerializedName(CC)
    @Json(name = CC)
    @JsonProperty(CC)
    private long cc;
    @SerializedName(AAA)
    @Json(name = AAA)
    @JsonProperty(AAA)
    private int aaa;
    @SerializedName(BBB)
    @Json(name = BBB)
    @JsonProperty(BBB)
    private int bbb;
    @SerializedName(CCC)
    @Json(name = CCC)
    @JsonProperty(CCC)
    private int ccc;
    @SerializedName(STRINGS)
    @Json(name = STRINGS)
    @JsonProperty(STRINGS)
    private List<String> strings;

    public ComplexChildObject() {
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public long getAa() {
        return aa;
    }

    public void setAa(long aa) {
        this.aa = aa;
    }

    public long getBb() {
        return bb;
    }

    public void setBb(long bb) {
        this.bb = bb;
    }

    public long getCc() {
        return cc;
    }

    public void setCc(long cc) {
        this.cc = cc;
    }

    public int getAaa() {
        return aaa;
    }

    public void setAaa(int aaa) {
        this.aaa = aaa;
    }

    public int getBbb() {
        return bbb;
    }

    public void setBbb(int bbb) {
        this.bbb = bbb;
    }

    public int getCcc() {
        return ccc;
    }

    public void setCcc(int ccc) {
        this.ccc = ccc;
    }

    public List<String> getStrings() {
        return strings;
    }

    public void setStrings(List<String> strings) {
        this.strings = strings;
    }

    @Override
    public String toString() {
        return "ComplexChildObject{" +
                "a='" + a + '\'' +
                ", b='" + b + '\'' +
                ", c='" + c + '\'' +
                ", aa=" + aa +
                ", bb=" + bb +
                ", cc=" + cc +
                ", aaa=" + aaa +
                ", bbb=" + bbb +
                ", ccc=" + ccc +
                ", strings=" + strings +
                '}';
    }
}
