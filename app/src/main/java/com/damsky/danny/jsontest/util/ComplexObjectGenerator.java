package com.damsky.danny.jsontest.util;

import com.damsky.danny.jsontest.data.ComplexChildObject;
import com.damsky.danny.jsontest.data.ComplexParentObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for generating a complex object to be used for running benchmarks
 * of the various JSON parsing solutions.
 *
 * @author Danny Damsky
 * @see ComplexParentObject
 * @see ComplexChildObject
 */
public final class ComplexObjectGenerator {

    private static final int CHILD_OBJECTS_AMOUNT = 1000;
    private static final int CHILD_OBJECT_STRINGS_AMOUNT = 100;

    /**
     * Builds a heavy object to be used for running benchmarks of the various
     * JSON parsing solutions.
     *
     * @return a heavy object.
     */
    public static ComplexParentObject buildComplexParentObject() {
        final ComplexParentObject complexParentObject = new ComplexParentObject();
        final String s = "aslkdjl213087109238172#*()&#^!)&#^)!#";
        final int i = Integer.MAX_VALUE;
        final long l = Long.MAX_VALUE;

        complexParentObject.setA(s);
        complexParentObject.setB(s);
        complexParentObject.setC(s);
        complexParentObject.setAa(l);
        complexParentObject.setBb(l);
        complexParentObject.setCc(l);
        complexParentObject.setAaa(i);
        complexParentObject.setBbb(i);
        complexParentObject.setCcc(i);

        final List<ComplexChildObject> childObjects = new ArrayList<>(CHILD_OBJECTS_AMOUNT);
        for (int j = 0; j < CHILD_OBJECTS_AMOUNT; j++) {
            final ComplexChildObject childObject = new ComplexChildObject();
            childObject.setA(s);
            childObject.setB(s);
            childObject.setC(s);
            childObject.setAa(l);
            childObject.setBb(l);
            childObject.setCc(l);
            childObject.setAaa(i);
            childObject.setBbb(i);
            childObject.setCcc(i);

            final List<String> childStrings = new ArrayList<>(CHILD_OBJECT_STRINGS_AMOUNT);
            for (int k = 0; k < CHILD_OBJECT_STRINGS_AMOUNT; k++) {
                childStrings.add(s);
            }

            childObject.setStrings(childStrings);
            childObjects.add(childObject);
        }
        complexParentObject.setChildren(childObjects);

        return complexParentObject;
    }

    private ComplexObjectGenerator() {
    }
}
