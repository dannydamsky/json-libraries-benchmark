package com.damsky.danny.jsontest.util.json;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Utility class for converting objects to and from JSON using the {@link Gson} implementation.
 *
 * @author Danny Damsky
 */
public final class GsonUtils {

    private static final Gson gson = new Gson();

    @NonNull
    public static <T> T fromJson(@NonNull final String json, @NonNull final Class<T> classOfT) throws JsonSyntaxException {
        return gson.fromJson(json, classOfT);
    }

    @NonNull
    public static String toJson(@NonNull final Object src) {
        return gson.toJson(src);
    }

    private GsonUtils() {
    }
}
